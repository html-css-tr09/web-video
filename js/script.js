// ASIGNO VARIABLES A LOS ELEMENTOS DEL DOM QUE QUIERO ATACAR
let video = document.querySelector('video');
let canvas = document.querySelector('canvas');
let capturar = document.querySelector('#capturar');
let monotone = document.querySelector('#monotone');
let lightness = document.querySelector('#sepia');
let blur = document.querySelector('#blur');
let reset = document.querySelector('#reset');

// VARIABLE DE ESTADO
let variable = false;

// VARIABLES
let context = canvas.getContext('2d');
let img = document.createElement('img');

// DIMENSIONES DEL CANVAS
canvas.width = canvas.scrollWidth;
canvas.height = canvas.scrollHeight;

// CAPTURO IMAGEN VIDEO
function pintarCanvas(){

    context.globalAlpha = 1;

    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    canvas.appendChild(img);

    // CAMBIO ESTADO DE LA VARIABLE
    variable = true;

    let datosimg = context.getImageData(0, 0, canvas.width, canvas.height);
    console.log(datosimg.data.length);
};


// FILTRO BLANCO Y NEGRO

function monotoneImg() {

    pintarCanvas();
    let datosimg = context.getImageData(0, 0, canvas.width, canvas.height);
    canvas.appendChild(img);

    // CAMBIO ESTADO DE LA VARIABLE
    if (variable != true) {
        variable = true;
    }

    // console.log(datosimg.data.length);

    const pixels = datosimg.data;
    let numPixels = datosimg.width * datosimg.height;
 
    for ( var i = 0; i < numPixels; i++ ) {

        //RECOGEMOS LOS COLORES DEL ARRAY Y EL ALPHA

        let r = pixels[ i * 4 ], // ROJO
            g = pixels[ i * 4 + 1 ], // VERDE
            b = pixels[ i * 4 + 2 ]; // AZUL
            b = pixels[ i * 4 + 3 ]; // ALFA

            // VARIABLE PARA GRIS
            let grey = ( r + g + b ) / 3;
 
            pixels[ i * 4 ] = grey;
            pixels[ i * 4 + 1 ] = grey;
            pixels[ i * 4 + 2 ] = grey;
    };

    context.putImageData(datosimg, 0, 0 );

}

// FILTRO SEPIA

function sepiaImg() {

    pintarCanvas();

    let datosimg = context.getImageData(0, 0, canvas.width, canvas.height);
    canvas.appendChild(img);

    // CAMBIO ESTADO DE LA VARIABLE
    if (variable != true) {
        variable = true;
    }

    const pixels = datosimg.data;
    let numPixels = datosimg.width * datosimg.height;
 
    for ( var i = 0; i < numPixels; i++ ) {
        var r = pixels[ i * 4 ];
        var g = pixels[ i * 4 + 1 ];
        var b = pixels[ i * 4 + 2 ];
 
        pixels[ i * 4 ] = 255 - r;
        pixels[ i * 4 + 1 ] = 255 - g;
        pixels[ i * 4 + 2 ] = 255 - b;
 
        pixels[ i * 4 ] = ( r * .393 ) + ( g *.769 ) + ( b * .189 );
        pixels[ i * 4 + 1 ] = ( r * .349 ) + ( g *.686 ) + ( b * .168 );
        pixels[ i * 4 + 2 ] = ( r * .272 ) + ( g *.534 ) + ( b * .131 );
    }

    context.putImageData(datosimg, 0, 0 );

}


// FILTRO BLUR

function blurImg(){

    pintarCanvas();

    // CAMBIO ESTADO DE LA VARIABLE
    if (variable != true) {
        variable = true;
    }

    context.globalAlpha = 0.3;

    let offset = 3;

    for (let i = 1; i <= 8; i+=1) {
        context.drawImage(canvas, offset, 0, canvas.width - offset, canvas.height, 0, 0, canvas.width-offset, canvas.height);
        context.drawImage(canvas, 0, offset, canvas.width, canvas.height - offset, 0, 0,canvas.width, canvas.height-offset);
    }
}


// FUNCIONALIDAD BOTONES

// CAPTURA
let hacerCaptura = capturar.addEventListener('click', function() {
    pintarCanvas();
})

// MONOTONO
let monotonefilter = monotone.addEventListener('click', function(){
    monotoneImg();
})

// SEPIA
let sepiafilter = sepia.addEventListener('click', function(){
    sepiaImg();
})

// DESENFOQUE GAUSIANO
let blurFilter = blur.addEventListener('click', function() {
    blurImg();
})

// RESETEO CANVAS
reset.onclick = function resetear() {
    if (variable) {
        let img = canvas.getContext("2d");
        img.clearRect(0, 0, canvas.width, canvas.height);

        variable = false;
    }
};

